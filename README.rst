mkc
===

Initialize a C++ project.

Installation
------------

From the project root directory::

    $ python setup.py install

Usage
-----

Simply run it::

    $ mkc $PROJECT_NAME

Use --help/-h to view info on the arguments::

    $ mkc --help

Release Notes
-------------

:0.0.1:
    Project created
